# codex

Codex is a code extractor for OASIS Open Document Format (ODF) documents.
It uses style names to find code listings (with captions) and
snippets (without captions). It extracts each listing or snippet to
its own text file. Typical use is to extract examples when writing
a book or manual so you can verify the examples.

See the man pages for user documentation.

Codex depends on libxml2 (widely available from GNOME or xmlsoft.org)
and ziplib (less often installed, but downloadable from http://www.nih.at/libzip/.

Visit httpis://gitlab.com/cpphelp/codex/ to download the latest
version and to obtain additional information.

Ray Lischner
ray@cpphelp.com
2020-04-05
