/***************************************************************************
 *   Copyright (C) 2006 by Ray Lischner                                    *
 *   codex@tempest-sw.com                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/** @file codex.cpp
 * Main program for the Code Extractor.
 */

/** @mainpage codex - The Code Extractor for OpenOffice.org
 * The codex Code Extractor reads an OpenOffice.org 2.0
 * document and extracts all the code listings and snippets
 * to separate text files.
 *
 * Codex identifies code listings and snippets based on
 * style names. This program works best with the style sheets
 * used by typical publishers.
 *
 * A listing starts with a paragraph that has a caption style.
 * The listing itself uses a code style. A configuration file
 * enumerates the names of caption and code styles. A default
 * configuration file uses the name "caption" for the caption
 * style and "code" or "Preformatted Text" as the code style.
 * Additional configuration files are available for the Apress
 * and O'Reilly style sheets.
 *
 * The name of the text file has the form
 * <tt>\<PREFIX\>\<C\>\<N\>\<EXT\></tt>, where @c \<PREFIX\>
 * is a text prefix (specified in the configuration file or
 * on the command line), @c \<C\> is the chapter number,
 * @c \<N\> is the listing number, and @c \<EXT\> is an
 * extension. The chapter and listing numbers come from the
 * caption, which is assumed to have the form
 * <tt>\<TAG\> \<C\>-\<N\>. \<TEXT\></tt>,
 * where @c \<TAG\> is a single word. The separator between
 * @c \<C\> and @c \<N\> can be any single non-numeric
 * character. The punctuation after @c \<N\> can be any
 * non-numeric character. The rest of @c \<TEXT\> is
 * unimportant, provided it takes a single paragraph.
 *
 * The extension is specified in the configuration file
 * and can be overridden on the command line. A single
 * listing can specify the extension for that file only
 * by including the extension as hidden text immediately
 * before the caption or snippet.
 *
 * An extension must include whatever punctuation you want.
 * Typically, the extension starts with a dot (<tt>.</tt>).
 *
 * A snippet is a code listing without a caption. The
 * configuration file specifies a separate prefix and
 * extension for snippets. The chapter number must be named
 * on the command line, or else it will not be used in the
 * snippet file names.
 *
 * If the user specifies a chapter number, codex can use
 * that number to verify that all the listings are using
 * the proper chapter number. When verifying captions,
 * the program also checks that the listings are numbered
 * sequentially, starting from one.
 */

#include "options.hpp"
#include "utils.hpp"
#include "xml.hpp"
#include "zip.hpp"

#include <cassert>
#include <cctype>
#include <cerrno>
#include <cstring>
#include <exception>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <memory>
#include <ostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <string_view>
#include <system_error>
#include <typeinfo>
#include <utility>

extern "C" {
#include <argp.h>
#include <libxml/tree.h>
}

using namespace std::literals::string_view_literals;

namespace codex
{

#define VERSION "1.0"
#define EMAIL_ADDRESS  "ray@cpphelp.com"

/** Convenient typedef for mapping strings to strings.
 * Sometimes OOo uses an internal name for styles, e.g.,
 * P1 instead of Code. Other times, a style uses special
 * characters, so OOo encodes the style name. In either case,
 * the user specifies the user's visible style name in the
 * configuration file, so that is the style that codex must
 * use. The stylemap dictionary, therefore, maps the encoded
 * style name to the true style name. Look up every style
 * name in the style map before comparing the name with a
 * name from the configuration file. Note further that
 * mappings can be multiple levels (e.g., OOo's automatic
 * mapping, plus a special-character encoding mapping).
  */
typedef std::map<std::string, std::string> dictionary;
dictionary stylemap; ///< dictionary for storing mapped style names

int snippet_num; ///< number of the snippet most recently extracted
int listing_num; ///< number of the listing most recently extracted

bool cmdline_chapter = false;     ///< whether the user supplied @c -n on the command line
bool cmdline_configfile = false;  ///< whether the user supplied @c -f on the command line
bool cmdline_snippets = false;    ///< whether the user supplied @c -s or @c -S on the command line
bool cmdline_doxygen = false;     ///< whether the user supplied @c -d or @c -D on the command line

/** Determine whether a style name is one of the Code styles.
 * @param name the style name to test
 * @param config the configuration data
 * @return true if @p name is a code style name
 */
bool is_code_style(std::string name, Config const& config)
{
  dictionary::iterator iter = stylemap.find(name);
  while (iter != stylemap.end())
  {
	name = iter->second;
	iter = stylemap.find(name);
  }

  return config.is_code_style(name);
}

/** Determine whether a style name is one of the Caption styles.
 * @param name the style name to test
 * @param config the configuration data
 * @return true if @p name is a caption style name
 */
bool is_caption_style(std::string name, Config const& config)
{
  dictionary::iterator iter = stylemap.find(name);
  while (iter != stylemap.end())
  {
	name = iter->second;
	iter = stylemap.find(name);
  }

  return config.is_caption_style(name);
}

/** Determine whether a style name is one of the styles to ignore.
 * @param name the style name to test
 * @param config the configuration data
 * @return true if @p name is an "ignore" style name
 */
bool is_ignore_style(std::string name, Config const& config)
{
  dictionary::iterator iter = stylemap.find(name);
  while (iter != stylemap.end())
  {
	name = iter->second;
	iter = stylemap.find(name);
  }

  return config.is_ignore_style(name);
}

/** Examine the attributes of a @c \<style\> tag to see if it is
 * mapping a paragraph style with an encoded name. If so, record
 * the mapping in the stylemap. The @c name attribute is the encoded
 * style name, and @c display-name is the user-visible name.
 * @param attr the first attribute of the @c \<style\> tag
 */
void extract_style(xmlAttr* attr)
{
  std::string display_name, family, name;

  for ( ; attr != nullptr; attr = attr->next)
  {
	if (xml::attr_is(attr, "name"))
	  name = xml::string(attr->children->content);
	else if (xml::attr_is(attr, "family"))
	  family = xml::string(attr->children->content);
	else if (xml::attr_is(attr, "display-name"))
	  display_name = xml::string(attr->children->content);
  }

  if (family == "paragraph" and not name.empty() and not display_name.empty())
	stylemap[name] = display_name;
}

/** Examine the attributes of a @c \<style\> tag to see if it is
 * mapping an internally-generated style name. If so, record
 * the mapping in the stylemap. The @c name attribute is the encoded
 * style name, and @c parent-style-name is the original name.
 * @param attr the first attribute of the @c \<style\> tag
 */
void extract_auto_style(xmlAttr* attr)
{
  std::string family, name, parent;

  for ( ; attr != nullptr; attr = attr->next)
  {
	if (xml::attr_is(attr, "name"))
	  name = xml::string(attr->children->content);
	else if (xml::attr_is(attr, "parent-style-name"))
	  parent = xml::string(attr->children->content);
	else if (xml::attr_is(attr, "family"))
	  family = xml::string(attr->children->content);
  }

  if (family == "paragraph" and not name.empty() and not parent.empty())
	stylemap[name] = parent;
}

/** Extract all the @c \<style\> tags in a @c \<styles\> section.
 * @param node the first @c \<style\> tag.
 */
void extract_styles(xmlNode* node)
{
  for ( ; node != nullptr; node = node->next)
	if (xml::node_is(node, "style"))
	  extract_style(node->properties);
}

/** Extract all the @c \<style\> tags in an @c \<automatic-styles\> section.
 * @param node the first @c \<style\> tag.
 */
void extract_auto_styles(xmlNode* node)
{
  for ( ; node != nullptr; node = node->next)
	if (xml::node_is(node, "style"))
	  extract_auto_style(node->properties);
}

/** Retrieve the name of a style node.
 * @param node the @c \<style\> node
 * @return the style name
 */
inline std::string get_style_name(xmlNode* node)
{
  return xml::get_attr_value(node, "style-name");
}

/** Get the chapter and listing number from a caption.
 * The caption can optionally start with white space,
 * followed by a word (such as Listing, Figure, or Example),
 * followed by more white space, and then the chapter number,
 * a separator character, and the listing number. If anything
 * goes wrong, use zero.
 * @param caption the caption to parse
 * @return a pair of integers; the first is the chapter and second is the listing number
 */
std::pair<int,int> getnum(std::string const& caption)
{
  std::istringstream in(caption);
  std::string tag;
  int chapter(0), number(0);
  char separator;
  in >> tag >> chapter >> separator >> number;
  return std::make_pair(chapter, number);
}

/** Get the number of spaces denoted by an @c \<s\> tag.
 * The @c "c" attribute specifies the value as an integer,
 * e.g., <tt>\<s c="4"/\></tt>. The default is 1 space (e.g., `<s/>`).
 * @param node the @c \<s\> node
 * @return an integer number of spaces that the <tt>\<s c="N"\></tt> node represents
 */
int get_num_spaces(xmlNode* node)
{
  std::istringstream in(xml::get_attr_value(node, "c"));
  int result{1};
  in >> result;
  return result;
}

/** Should the program ignore this node?
 * Ignore paragraphs that have an ignore style.
 * Ignore hidden and deleted text.
 * @param node the node to test
 * @param config the configuration
 * @returns true to ignore, false to process normally
 */
bool ignore_node(xmlNode* node, Config const& config)
{
  return xml::node_is(node, "hidden-text") or
		 xml::node_is(node, "deletion")    or
		 xml::node_is(node, "change-info") or
		 xml::node_is(node, "annotation") or
		 is_ignore_style(get_style_name(node), config);
}

/** Extract the text of a code listing.
 * The text can have text nodes, @c \<s\> nodes,
 * various change tags, span tags, and so on. Thus,
 * extracting the text is recursive.
 *
 * The program currently uses narrow characters, and
 * therefore simply copies the UTF-8 characters from the input
 * to the output string.
 * @param node the first Code node to examine
 * @param config Reference to the configuration
 * @return A string that contains the net text of the line.
 */
std::string extract_code_text(xmlNode* node, Config const& config)
{
  std::string result;
  for ( ; node != nullptr ; node = node->next)
  {
	if (not ignore_node(node, config))
	{
	  if (xml::node_is(node, "s"))
		result.resize(result.size() + get_num_spaces(node), ' ');
	  if (xmlNodeIsText(node))
		result += xml::charptr(node->content);
	  result += extract_code_text(node->children, config);
	}
  }
  return result;
}

/** Extract a code listing.
 * A code listing is a series of paragraphs
 * (<tt>\<p\></tt> nodes) that have code styles.
 * The code style paragraphs might have interleaved
 * paragraphs with an ignore style. The extracted
 * code listing will have only the paragraphs with
 * code styles.
 *
 * The listing ends with the first <tt>\<p\></tt>
 * node with a non-code, non-ignored style. The styles
 * can be any mix of code styles; they do not all have
 * to be the same style.
 *
 * The program currently uses narrow characters, and
 * therefore simply copies the UTF-8 characters from the input
 * to the output. Thus, it works only with ostream, and not
 * with wostream or any templated form of basic_ostream.
 * @param node the first Code node to examine
 * @param config the configuration data
 * @param out the output stream
 * @returns the first non-code <tt>\<p\></tt> node
 */
xmlNode* extract_code(xmlNode* node, Config const& config, std::vector<std::string>& pars)
{
  for ( ; node != nullptr ; node = node->next)
  {
	if (xml::node_is(node, "p"))
	{
	  if (is_code_style(get_style_name(node), config))
	  {
		std::string text{ extract_code_text(node->children, config) };
		pars.emplace_back(std::move(text));
	  }
	  else if (not ignore_node(node, config))
		// Not a code or ignore style, so the listing or snippet has ended.
		break;
	}
	else if (not ignore_node(node, config))
	  extract_code(node->children, config, pars);
  }

  return node;
}

/** Get the caption of a code listing.
 * The caption can have spans and whatnot, so recursively
 * extract all the text to be the caption. Also look for
 * hidden-text nodes, which can specify a different filename.
 * Modify the @p name only if the hidden-text is present
 * and contains a string.
 * @param node The first child of the @c \<p\> node that starts the caption
 * @param[in,out] caption append the text to this string as text is recursively extracted
 * @param[in,out] name store the file name here, if one is present
 */
void get_caption(xmlNode* node, Config const& config, std::string& caption, std::string& name)
{
  for ( ; node != nullptr ; node = node->next)
  {
	if (xml::node_is(node, "hidden-text"))
	{
	  std::string tmp = xml::get_attr_value(node, "string-value");
	  if (not tmp.empty())
		name = tmp;
	}
	else if (xmlNodeIsText(node))
	  caption += xml::charptr(node->content);
	if (not ignore_node(node, config))
	  get_caption(node->children, config, caption, name);
  }
}

/** Write a file.
 * If the file already exists, read it and compare with the
 * new contents. Write the file only if the contents
 * change or if the file dild not already exist.
 * @param filename
 * @param contents
 */
void write_file(std::string const& filename, std::string_view contents)
{
	std::string previous_contents;
	std::ifstream input(filename);
	if (input)
	{
		std::ostringstream stream;
		stream << input.rdbuf();
		previous_contents = std::move(stream).str();
	}
	if (previous_contents != contents)
	{
		std::ofstream output(filename);
		if (output)
			output << contents;
		if (not output)
			throw std::system_error(errno, std::system_category(), filename);
		if (options.verbose() > 1)
			std::cout << "  " << filename << " written.\n";
	}
	else if (options.verbose() > 2)
		std::cout << "  " << filename << " not changed.\n";
}

/** Check every line in @p lines for a line number.
 * Returns the size of the line number, which must be the same
 * on every line. If a line does not have a line number,
 * or if the regex matches a different number of characters
 * on any line, then returns zero to mean no line number.
 *
 * The line number regex must have a capture group.
 * The capture group is the line number itself. The regex
 * may match additional text, such as whitespace surrounding
 * the line number. The capture group must match on every line
 * and have the same size. The full regex may or may not match
 * additional text, all of which will be erased if every line
 * has a line number that matches the capture group.
 */
std::string::difference_type match_line_numbers(std::vector<std::string> const& lines, std::regex const& regex)
{
	std::string::difference_type match_size{0};
	std::smatch match;
	for (auto&& line : lines)
	{
		if (not std::regex_search(line, match, regex))
			// No line number.
			return 0;
		else
		{
			if (match_size == 0)
			    match_size = match[1].length();
			else if (match_size != match[1].length())
			    // What appeared to be line numbers are actually
			    // something else. This listing may be data, for example.
			    return 0
			    ;
		}
	}

	return match_size;
}

/** If line numbers are present, erase them.
 * @see match_line_numbers()
 */
void erase_line_numbers(std::vector<std::string>& lines, std::regex const& regex)
{
	for (auto& line : lines)
	{
		static const std::string replace;
		line = std::regex_replace(line, regex, replace);
	}
}

/** Extract one code listing.
 * Get the caption, possibly with a new extension. Then parse the
 * caption to get the chapter and listing numbers. Verify the numbers
 * if the user requested it. Then format the file name, open the file,
 * and extract the code.
 * @param node The <tt>\<p\></tt> node that starts the caption
 * @param config the configuration data
 * @returns the first non-code <tt>\<p\></tt> node after the code listing
 */
xmlNode* extract_code(xmlNode* node, Config const& config)
{
  std::string caption;
  std::string name;

  // Get the caption text, which might also contain a different
  // file name.
  get_caption(node->children, config, caption, name);
  // Parse the chapter and listing numbers from the caption.
  std::pair<int,int> num = getnum(caption);

  ++listing_num;
  if (options.warning())
  {
	// Verify the chapter number.
	if (options.have_chapter() and options.chapter() != num.first)
	  std::cerr << "warning: Expected chapter " << options.chapter() << " for " << caption << '\n';
	// Verify the sequence of listing numbers.
	if (listing_num != num.second)
	{
	  std::cerr << "warning: Expected listing " << listing_num << ", but got " << num.second << '\n';
	  listing_num = num.second;
	}
  }

  // Format the listing file name.
  // If the caption has no hidden text, get the extension from the
  // options, and format a file name from the listing number.
  if (name.empty() or name.front() == '.') {
	std::string extension{ name.empty() ? options.extension() : std::move(name) };
    std::ostringstream stream;
    stream << options.prefix()
		<< std::setw(2) << std::setfill('0') << num.first << std::setw(2) << num.second
		<< extension;
    name = std::move(stream).str();
  }

  if (options.verbose() > 1)
	std::cout << "  " << name;
  if (options.verbose() > 2)
	std::cout << ": " << caption;
  if (options.verbose() > 1)
	std::cout << '\n';

  // Copy
  std::ostringstream comment;
  if (not options.comment_start().empty())
  {
	if (options.doxygen())
	  comment << options.comment_start() << options.file_tag() << ' '
		  << name << options.comment_end();
	comment << options.comment_start() << caption << options.comment_end();
  }

  // Collect every line of text in the listing.
  std::vector<std::string> lines;
  auto result{ extract_code(node->next, config, lines) };
  // Check every line for a line number. Check that the matching text
  // has the same size on every line.
  auto match_size{ match_line_numbers(lines, config.line_number_regex()) };

  // Collect all the lines, stripping line numbers if needed.
  std::string text{ std::move(comment).str() };
  if (match_size != 0)
	erase_line_numbers(lines, config.line_number_regex());

  for (auto& line : lines)
  {
    config.rewrite(line);
    text += line;
    text += '\n';
  }

  // Finally, extract the body of the listing to the file.
  write_file(name, text);
  return result;
}

/** Look for hidden text that specifies an alternate file name
 * for a code snippet.
 * @param parent the first @c \<p\> node for the code snippet
 * @param[in,out] name store the name here if one is found
 * @returns true if a name is found, false otherwise
 */
bool get_snippet_name(xmlNode* parent, std::string& name)
{
  for (xmlNode* node = parent->children ; node != nullptr; node = node->next)
	if (xml::node_is(node, "hidden-text"))
	{
	  std::string tmp = xml::get_attr_value(node, "string-value");
	  if (not tmp.empty())
		name = std::move(tmp);
	  return true;
	}
	else if (get_snippet_name(node, name))
	  return true;
  return false;
}

/** Extract a code snippet. Look for hidden text for an alternate
 * extension, and format the file name. Then extract all adjacent
 * code paragraphs to the snippet file.
 * @param node the first <tt>\<p\></tt> node with a code style
 * @param config the configuration data
 * @returns the first <tt>\<p\></tt> node with a non-code style after the snippet
 */
xmlNode* extract_snippet(xmlNode* node, Config const& config)
{
  // Then see if the document overrides the file name.
  std::string name;
  get_snippet_name(node, name);
  if (name.empty() or name.front() == '.')
  {
    // Start by getting the expected file name extension.
    std::string extension{name.empty() ? options.snippet_extension() : std::move(name)};

    // Format the snippet file name.
    ++snippet_num;
    std::ostringstream stream;
    stream << options.snippet_prefix() << std::setfill('0');
    if (options.have_chapter())
	  stream << std::setw(2) << options.chapter();
    stream << std::setw(2) << snippet_num << extension;
    name = std::move(stream).str();
  }

  std::ostringstream comment;

  // If the user wants a comment, put the snippet number in the file.
  if (not options.comment_start().empty())
  {
	if (options.doxygen())
	  comment << options.comment_start() << options.file_tag() << ' '
		  << name << options.comment_end();
	comment << options.comment_start() << "Code Snippet ";
	if (options.have_chapter())
	  comment << options.chapter() << '-';
	comment << snippet_num << options.comment_end();
  }
  std::string text{ std::move(comment).str() };

  // Finally extract the body of the snippet.
  std::vector<std::string> lines;
  auto result{ extract_code(node, config, lines) };
  for (auto& line : lines) {
      config.rewrite(line);
	  text += line;
	  text += '\n';
  }
  write_file(name, text);
  return result;
}

/** Examine the metadata nodes, looking for user-defined document properties.
 * The document can override some command line options with
 * user-defined document properties. Each property name must
 * be a recognized @em codex option name, which just happen to
 * the same as command line option names with @c codex- before the name.
 * the property value is the new value for that option:
 *   - @c codex-configfile
 *     @em filename
 *   - @c codex-chapter-number
 *     @em number
 *   - @c codex-snippets
 *     (no argument)
 *   - @c codex-nosnippets
 *     (no argument)
 *
 * The command line overrides the document, so these settings are used
 * only if they were not specified on the command line. Because these
 * properties are not immediately obvious to the user, and user might
 * even have forgotten about them, the name of the configuration file
 * and the chapter number are echoed (if the user has verbosity set).
 *
 * If the property name is not recognized and starts with @c codex- then
 * a warning is issued in case the user misspelled a property name. Other
 * unknown properties are ignored.
 *
 * @param[in] meta The parent @c \<meta\> node
 * @param pathname the name of the archive and document, for error messages
 */
void extract_meta(xmlNode* meta, std::string const&)
{
  for (xmlNode* node = meta->children ; node != nullptr; node = node->next)
	if (xml::node_is(node, "user-defined"))
	{
	  std::string name = xml::get_attr_value(node, "name");
	  if (name == options.config().config_file_propname())
	  {
		if (not cmdline_configfile)
		{
		  options.configfile(xml::get_content(node));
		  if (options.verbose() > 0)
			std::cout << "configfile: " << options.configfile() << '\n';
		}
	  }
	  else if (name == options.config().chapter_number_propname())
	  {
		if (not cmdline_chapter)
		{
		  options.chapter(xml::get_content(node).c_str());
		  if (options.verbose() > 0)
			std::cout << "chapter-number: " << options.chapter() << '\n';
		}
	  }
	  else if (name == options.config().doxygen_propname())
	  {
		if (not cmdline_doxygen)
		{
		  auto doxygen{ xml::get_content(node) };
		  options.doxygen(istrue(doxygen.c_str()));
		}
	  }
	  else if (name == options.config().snippets_propname())
	  {
		if (not cmdline_snippets)
		{
		  auto snippets{ xml::get_content(node) };
		  options.snippets(istrue(snippets.c_str()));
		}
	  }
	  // else ignore the user-defined property
	}
}

/** Parse the metadata to find document properties.
 * @param f the metadata zip file
 */
void parse_meta(Zip::File& f)
{
  std::string text(f.read());
  xml::doc doc;
  doc.parse(text);
  // The root element is <document:meta>, and its child is <meta>.
  extract_meta(doc.get_root_element()->children, f.pathname());
}

/** Extract the code listings and snippets from the document body.
 * @param node the @c \<body\> node, which encloses all
 *             the @c \<p\> nodes in the document
 * @param config the configuration data
 */
void extract_body(xmlNode* node, Config& config)
{
	while (node != nullptr)
	{
		if (xml::node_is(node, "p"))
		{
			std::string style = get_style_name(node);
			if (is_caption_style(style, config))
			{
			  node = extract_code(node, config);
			  if (node == nullptr)
				return;
			}
			else if (is_code_style(style, config))
			{
			  if (not options.snippets())
				return;
			  node = extract_snippet(node, config);
			  if (node == nullptr)
				return;
			}
			else {
				extract_body(node->children, config);
				node = node->next;
			}
		}
		else if (xml::node_is(node, "deletion") or xml::node_is(node, "change-info"))
			// These nodes contain text, but that text should not be extracted.
			return;
		else {
			extract_body(node->children, config);
			node = node->next;
		}
	}
}

/** Extract the contents of a document.
 * Extract styles, listings, and snippets. This function
 * loops through a node list, but typically the document
 * contains exactly one node.
 * @param parent the document root element
 * @param config the configuration data
 */
void extract(xmlNode* parent, Config& config)
{
  // parent is document-styles or document-content.
  for (xmlNode* node = parent->children ; node != nullptr; node = node->next)
	if (xml::node_is(node, "automatic-styles"))
	  extract_auto_styles(node->children);
	else if (xml::node_is(node, "styles"))
	  extract_styles(node->children);
	else if (xml::node_is(node, "body"))
	  extract_body(node, config);
}

/** Parse an XML stream into a tree, and then walk the tree,
 * extracting data. The entire stream is read into memory,
 * and the entire XML tree resides in memory. In theory, one
 * could rearchitect the program to decompress a stream and
 * parse it on the fly, but it's not worth the trouble. Any
 * real-world document will fit comfortably in memory on any
 * real-world system.
 * @param f the file to parse and extract
 */
void parse_file(Zip::File& f)
{
  std::string text(f.read());
  xml::doc doc;
  Config& config = options.config();
  doc.parse(text);
  extract(doc.get_root_element(), config);
}

/** Process a single document. Extract all the styles to
 * build the style map, then extract the listings and
 * snippets from the document content.
 * @param filename the name of the document file
 */
void do_file(char const* filename)
{
  if (options.verbose() > 0)
	std::cout << filename << '\n';
  Zip::Archive zip(filename);

  snippet_num = 0;
  listing_num = 0;
  stylemap.clear();

  Zip::File meta(zip, "meta.xml");
  parse_meta(meta);
  meta.close();

  Zip::File styles(zip, "styles.xml");
  parse_file(styles);
  styles.close();

  Zip::File content(zip, "content.xml");
  parse_file(content);
  content.close();

  zip.close();
}

/** Command line argument parser. The ARGP package calls back
 * to this function for every command line argument.
 * @param key the command line option or a magic ARGP value
 * @param arg the argument to a command line option, if present
 * @param state internal state for the ARGP processor
 * @returns 0 for success or @c ARGP_ERR_UNKNOWN for any error
 */
extern "C" error_t parse_func(int key, char *arg, struct argp_state *state)
{
  switch (key)
  {
	case 'C':
	  options.comment_end(arg);
	  break;
	case 'c':
	  options.comment_start(arg);
	  break;
	case 'd':
	  options.doxygen(true);
	  cmdline_doxygen = true;
	  break;
	case 'D':
	  options.doxygen(false);
	  cmdline_doxygen = true;
	  break;
	case 'e':
	  options.extension(arg);
	  break;
	case 'f':
	  options.configfile(arg);
	  cmdline_configfile = true;
	  break;
	case 'n':
	  options.chapter(arg);
	  cmdline_chapter = true;
	  break;
	case 'p':
	  options.prefix(arg);
	  break;
	case 'q':
	  options.be_quiet();
	  break;
	case 'r':
	  options.snippet_prefix(arg);
	  break;
	case 's':
	  options.snippets(true);
	  cmdline_snippets = true;
	  break;
	case 'S':
	  options.snippets(false);
	  cmdline_snippets = true;
	  break;
	case 't':
	  options.file_tag(arg);
	  break;
	case 'v':
	  options.increment_verbose();
	  break;
	case 'V':
	  std::cout << "codex " VERSION "\n"
		  "Copyright (c) 2020 Ray Lischner <" EMAIL_ADDRESS ">\n"
		  "This is free software; see the source for copying conditions.  There is NO\n"
		  "warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n";
	  std::exit(EXIT_SUCCESS);
	case 'w':
	  options.warning(true);
	  break;
	case 'W':
	  options.warning(false);
	  break;
	case 'x':
	  options.snippet_extension(arg);
	  break;
	case ARGP_KEY_ARG:
	  do_file(arg);
	  break;
	case ARGP_KEY_INIT:
	case ARGP_KEY_END:
	case ARGP_KEY_SUCCESS:
	case ARGP_KEY_FINI:
	  break;
	case ARGP_KEY_NO_ARGS:
	  argp_usage(state); // does not return
	  [[fallthrough]];
	default:
	  return ARGP_ERR_UNKNOWN;
  }
  return 0;
}

} // end of namespace

/** The main program. As you can see, the main program is pretty simple.
 * It sets up some XML stuff, then calls on ARGP to process the command line.
 * @param argc number of command line arguments
 * @param argv the command line arguments
 * @returns 0 for success, EXIT_FAILURE if anything goes wrong.
 */
int main(int argc, char *argv[])
{
  static argp_option options[] = {
	{ "chapter-number",    'n', "NUMBER", 0, "chapter number for snippet files and to verify listing", 0 },
	{ "comment-start",     'c', "STRING", 0, "characters that start a comment", 0 },
	{ "comment-end",       'C', "STRING", 0, "characters that end a comment", 0 },
	{ "configfile",        'f', "FILE",   0, "specify a configuration file", 0 },
	{ "doxygen",           'd', 0,        0, "Write doxygen @file comment", 0 },
	{ "nodoxygen",         'D', 0,        0, "Do not write doxygen @file comment (default)", 0 },
	{ "extension",         'e', "EXT",    0, "extension for generated file names", 0 },
	{ "file-tag",          't', "STRING", 0, "Specify a doxygen file tag (default is @file)", 0 },
	{ "prefix",            'p', "PREFIX", 0, "prefix for generated file names", 0 },
	{ "quiet",             'q', 0,        0, "turn off verbosity", 0 },
	{ "noverbose",         'q', 0,        OPTION_ALIAS, "", 0 },
	{ "snippets",          's', 0,        0, "enable extracting snippets (default)", 0 },
	{ "nosnippets",        'S', 0,        0, "disable extracting snippets; extract listings only", 0 },
	{ "snippet-prefix",    'r', "PREFIX", 0, "prefix for snippet filenames", 0 },
	{ "verbose",           'v', 0,        0, "echo file names as they are processed (default); "
											 "when repeated, also echoes generated file names", 0 },
	{ "version",           'V', 0,        0, "print version number and exit", 0 },
	{ "warning",           'w', 0,        0, "enable checking for valid listing numbers (default)", 0 },
	{ "nowarning",         'W', 0,        0, "disable checking for valid listing numbers", 0 },
	{ "snippet-extension", 'x', "EXT",    0, "extension for snippet filenames", 0 },
	{ 0, 0, 0, 0, 0, 0 }
  };

  static argp parse_info = { options, codex::parse_func, "DOCUMENTS...",
	"The Code Extractor finds code listings and snippets in an "
		" Open Document Format file, using style names as a guide, "
		"and extracts them to text files.\v"
		"Each file name named on the command line is opened as "
		"an Open Document Format file, that is, as a ZIP file "
		"that contains XML streams. The main content stream (content.xml) "
		"is extracted according to the OASIS OpenDocument format standard. "
		"Sections that use code style names are extracted to text files. "
		"A code listing starts with a caption style, followed by code styles. "
		"A snippet lacks a caption. Snippets are usually short."
		"\n\n"
		"Each listing or snippet is extracted to a separate text file in "
		"the current directory. The file name is formed from a prefix, "
		"the listing number, and an extension. Snippets and listings have "
		"different prefixes and extensions. Listings captions must have "
		"the form <TAG CC-NN. CAPTION>, where TAG is a word such as "
		"Listing, Example, Figure, etc. CC is the chapter number. "
		"NN is the listing number. CAPTION is arbitrary text. "
		"The separator between CC and NN can be any single character. "
		"The separator between NN and CAPTION can be anything. "
		"The listing filename is <PREFIX><CC><NN>.<EXT>, where CC and NN "
		"are taken from the caption and padded to 2 zero-filled places. "
		"If you supply a comment start string, the first line of the file "
		"starts with a comment for the caption. If the comment end string"
		"is empty, the comment ends with a newline. If you also request"
		"a doxygen-style @file comment, that precedes the caption comment,"
		"and the comment end string is doubled to two newlines, to clearly"
		"separate the brief caption comment from the description comment"
		"that presumably appears in the code listing."
		"\n\n"
		"Snippets lack a caption, so they are numbered sequentially, "
		"starting at 1. You can specify a chapter number, in which case "
		"that number is used to name snippet files. The chapter number "
		"can also be used to verify the correctness of listings. "
		"\n\n"
		"The style names come from a configuration file. "
		"The configuration file can also provide default values for "
		"extension, prefix, and other options. The command line "
		"overrides values from the configuration file. "
		"The file format is XML. The schema is provided in a separate "
		"file that accompanies this program. If the file name "
		"cannot be found in the local directory, a standard prefix "
		"is prepended, to look in a system directory.",
		nullptr,
		nullptr,
		nullptr
  };

  LIBXML_TEST_VERSION;
  xml::relax_ng rng;
  xml::parser parser;
  try {
	argp_parse(&parse_info, argc, argv, 0, 0, 0);
  } catch(std::exception& ex) {
	std::cerr << ex.what() << '\n';
	exit(EXIT_FAILURE);
  }
}
