/***************************************************************************
 *   Copyright (C) 2006 by Ray Lischner                                    *
 *   codex@tempest-sw.com                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "utils.hpp"
#include "xml.hpp"

#include <strings.h> // for strcasecmp
#include <cstdlib>
#include <fstream>
#include <ostream>

/** Determine whether two strings are equal, ignoring case.
 * The first string is unsigned char because that's what xmlChar is
 * in libxml, and that's the primary use of this function.
 * This function assumes @p y is ASCII. Thus, even if @p x
 * contains non-ASCII, high-bit UTF-8 characters, they will compare
 * unequal. Thus, a narrow strcasecmp function is adequate.
 * @param x the xmlChar string
 * @param y the string to test (typically a literal)
 * @returns true if the strings are equal, ignore case differences
 */
bool strequal(unsigned char const* x, char const* y)
{
  return strcasecmp(xml::charptr(x), y) == 0;
}

/// Indicate whether a string means "true".
/// @param arg a command line argument
/// @returns true if @p arg means true, false otherwise
bool istrue(char const* arg)
{
  return strcasecmp(arg, "yes")  == 0 or
	  strcasecmp(arg, "true") == 0 or
	  strcasecmp(arg, "on")   == 0;
}

/// Indicate whether a string means "false".
/// @param arg a command line argument
/// @returns true if @p arg means false, false otherwise
bool isfalse(char const* arg)
{
  return strcasecmp(arg, "no")  == 0 or
	  strcasecmp(arg, "false") == 0 or
	  strcasecmp(arg, "off")   == 0;
}

/** Copy a file.
 * @param from name of the source file
 * @param to name of the destination file
 */
void copyfile(std::string const& from, std::string const& to)
{
  std::ifstream in(from.c_str());
  std::ofstream out(to.c_str());
  if (not (out << in.rdbuf()))
  {
	std::perror(to.c_str());
	std::exit(EXIT_FAILURE);
  }
}
