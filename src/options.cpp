/***************************************************************************
 *   Copyright (C) 2006 by Ray Lischner                                    *
 *   codex@tempest-sw.com                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/// @file options.cpp
/// Implement the Options class

#include <sstream>
#include <string_view>
#include "options.hpp"

Options options; ///< Global, singleton instance of Options.

Config& Options::config()
const
{
  if (not config_)
	config_.reset(new Config());
  return *config_.get();
}

void Options::configfile(std::string const& cf)
{
  configfile_ = cf;
  config_.reset(new Config(cf.c_str()));
}

void Options::chapter(char const* arg)
{
  std::istringstream in(arg);
  int chap;
  if (in >> chap and chap >= 0)
	chapter(chap);
}

/* Return the comment end text.
 * If the user supplies a non-empty string, use it, followed by a newline.
 * Otherwise, use a newline. If the user requested doxygen formatting,
 * use two newlines. That separates the brief description, which is the
 * caption from the description, which the user presumably supplies in
 * the code listing.
 */
std::string Options::comment_end()
const
{
  static const std::string newline("\n");
  std::string tmp;
  if (comment_end_.empty())
	tmp = config().comment_end();
  else
	tmp = comment_end_;

  if (not tmp.empty())
	return tmp + newline;
  else if (doxygen())
	return newline + newline;
  else
	return newline;
}

std::string const& Options::comment_start()
const
{
  if (comment_start_.empty())
	return config().comment_start();
  else
	return comment_start_;
}

bool Options::doxygen()
const
{
  if (not setdoxygen_)
	return config().doxygen();
  else
	return doxygen_;
}

std::string const& Options::extension()
const
{
  if (extension_.empty())
	return config().extension();
  else
	return extension_;
}

std::string const& Options::file_tag()
const
{
  if (file_tag_.empty())
	return config().file_tag();
  else
	return file_tag_;
}

std::string const& Options::prefix()
const
{
  if (prefix_.empty())
	return config().prefix();
  else
	return prefix_;
}

std::string const& Options::snippet_extension()
const
{
  if (snippet_extension_.empty())
	return config().snippet_extension();
  else
	return snippet_extension_;
}

std::string const& Options::snippet_prefix()
	const
{
  if (snippet_prefix_.empty())
	return config().snippet_prefix();
  else
	return snippet_prefix_;
}
