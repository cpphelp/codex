/***************************************************************************
 *   Copyright (C) 2006 by Ray Lischner                                    *
 *   codex@tempest-sw.com                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/** @file configschema.hpp
 * Hardcode the schema for the configuration file.
 */

#ifndef CONFIGSCHEMA_HPP
#define CONFIGSCHEMA_HPP

/// The schema for the configuration file.
#define SCHEMA "<?xml version='1.0' encoding='UTF-8'?>\
<element name='config' xmlns='http://relaxng.org/ns/structure/1.0'>\
 <attribute name='version'/>\
 <interleave>\
  <zeroOrMore><element name='caption'>          <text/></element></zeroOrMore>\
  <zeroOrMore><element name='ignore'>           <text/></element></zeroOrMore>\
  <oneOrMore> <element name='code'>             <text/></element></oneOrMore>\
  <optional>  <element name='line-number-regex'><text/></element></optional>\
  <optional>  <element name='extension'>        <text/></element></optional>\
  <optional>  <element name='prefix'>           <text/></element></optional>\
  <optional>  <element name='snippet-prefix'>   <text/></element></optional>\
  <optional>  <element name='snippet-extension'><text/></element></optional>\
  <optional>  <element name='comment-end'>      <text/></element></optional>\
  <optional>  <element name='comment-start'>    <text/></element></optional>\
  <optional>  <element name='doxygen'>          <empty/></element></optional>\
  <zeroOrMore><element name='rewrite-line'>\
	<element name='match'><text/></element>\
	<element name='replace'><text/></element>\
  </element></zeroOrMore>\
  <optional>  <element name='property-names'>   <empty/>\
	<optional><attribute name='chapter-number'/></optional>\
	<optional><attribute name='doxygen'/></optional>\
	<optional><attribute name='snippets'/></optional>\
	<optional><attribute name='config-file'/></optional>\
  </element></optional>\
 </interleave>\
</element>"

#endif
