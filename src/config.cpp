/***************************************************************************
 *   Copyright (C) 2006 by Ray Lischner                                    *
 *   codex@tempest-sw.com                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/// @file config.cpp
/// Implement the Config class.

#include "config.hpp"
#include "configschema.hpp"
#include "utils.hpp"
#include "xml.hpp"

#include <algorithm>
#include <cassert>
#include <cstring>
#include <fstream>
#include <stdexcept>
#include <string>

extern "C" {
#include <libxml/tree.h>
}

#ifndef SYSTEM_DIR
/// Directory to search for configuration files.
/// The value is set by the configure script, stored in the Makefile,
/// and defined on the compiler command line. This default value
/// is here in case the user cannot use configure for some reason.
///
/// TODO: Portability to non-POSIX platforms.
# define SYSTEM_DIR "/usr/local/share/codex"
#endif

#ifndef DEFAULT_CONFIGFILE
/// Complete path to the default configuration file in the system directory.
/// Concatenate the system directory with the file name. Make sure
/// a path separator is included.
///
/// TODO: Portability to non-POSIX platforms.
# define DEFAULT_CONFIGFILE SYSTEM_DIR "/default.xml"
#endif

char const Config::default_configfile[] = DEFAULT_CONFIGFILE; ///< complete path to default configuration file
char const Config::schema[] = SCHEMA;                         ///< Relax-ng schema for configuration files
char const Config::version[] = "1.3";                         ///< version number of the schema
char const old_version[] = "1.2";                             ///< previous version number
char const Config::system_dir[] = SYSTEM_DIR;                 ///< system directory to search for configuration files
const char Config::default_file_tag[] = "@file";              ///< Default file tag

namespace {
  /** Open a configuration file.
   * Try to open @p filename for reading. If that fails,
   * prepend the system directory to the file name and
   * try again. If the file cannot be opened, return false.
   * Otherwise, then try to parse the XML contents of the file.
   * @param[out] doc The XML document to initialize
   * @param filename the name of the configuration file
   * @returns true for success or false for failure
   */
  bool open_configfile(xml::doc& doc, std::string filename)
  {
	std::ifstream in(filename.c_str());
	if (not in)
	{
	  // TODO: use std::filesystem for portability
	  filename = std::string(Config::system_dir) + '/' + filename;
	  in.open(filename.c_str());
	  if (not in)
		return false;
	}
	in.close();
	return doc.parse_entity(filename);
  }
}

Config::Config(char const* filename)
: doxygen_(false), file_tag_(default_file_tag)
{
  open(filename);
}

std::string error_msg(char const* filename, char const* msg)
{
  return std::string(filename) + ": " + msg;
}

void Config::open(char const* filename)
{
  xml::doc doc;
  if (not open_configfile(doc, filename))
	throw std::runtime_error(error_msg(filename, "Cannot parse configuration file"));

  xml::rng_parser_context context(schema, sizeof(schema)-1);
  if (not context)
	throw std::runtime_error(error_msg(filename, "Cannot create parser context"));

  xml::rng_schema schema(context);
  if (not schema)
	throw std::runtime_error(error_msg(filename, "Cannot parse schema"));

  xml::rng_validation_context validater(schema);
  if (not validater)
	throw std::runtime_error(error_msg(filename, "Cannot create schema validater"));

  if (not validater.is_valid(doc))
	throw std::runtime_error(error_msg(filename, "Configuration file is invalid"));

  xmlNode* config = doc.get_root_element();
  assert(config != 0);
  if (xml::get_attr_value(config, "version") != version and
	  xml::get_attr_value(config, "version") != old_version)
  {
	throw std::runtime_error(error_msg(filename, "Configuration file has wrong version number"));
  }

  load_config_data(doc);
}

bool Config::is_style(std::string const& name, styleset const& styles)
const
{
  auto iter{ std::find(styles.begin(), styles.end(), name) };
  return iter != styles.end();
}

void Config::add_style(std::string const& name, styleset& styles)
{
  styles.insert(name);
}

void Config::clear()
{
  comment_end_.clear();
  comment_start_.clear();

  doxygen_ = false;
  file_tag_ = default_file_tag;

  ext_.clear();
  prefix_.clear();
  snippet_extension_.clear();
  snippet_prefix_.clear();
  code_styles_.clear();
  caption_styles_.clear();
  ignore_styles_.clear();
  std::regex{}.swap(line_number_regex_);
  chapter_number_propname_.clear();
  doxygen_propname_.clear();
  snippets_propname_.clear();
  config_file_propname_.clear();
}

void Config::load_config_data(xml::doc& doc)
{
  clear();
  for (xmlNode* node = doc.get_root_element()->children; node != nullptr; node = node->next)
  {
	if (node->type == XML_COMMENT_NODE)
	  continue;
	else if (strequal(node->name, "caption"))
	{
	  assert(xmlNodeIsText(node->children));
	  add_caption_style(xml::string(node->children->content));
	}
	else if (strequal(node->name, "code"))
	{
	  assert(xmlNodeIsText(node->children));
	  add_code_style(xml::string(node->children->content));
	}
	else if (strequal(node->name, "comment-end"))
	{
	  assert(xmlNodeIsText(node->children));
	  comment_end(xml::string(node->children->content));
	}
	else if (strequal(node->name, "comment-start"))
	{
	  assert(xmlNodeIsText(node->children));
	  comment_start(xml::string(node->children->content));
	}
	else if (strequal(node->name, "doxygen"))
	  doxygen(true);
	else if (strequal(node->name, "extension"))
	{
	  assert(xmlNodeIsText(node->children));
	  extension(xml::string(node->children->content));
	}
	else if (strequal(node->name, "file-tag"))
	{
	  assert(xmlNodeIsText(node->children));
	  file_tag(xml::string(node->children->content));
	}
	else if (strequal(node->name, "ignore"))
	{
	  assert(xmlNodeIsText(node->children));
	  add_ignore_style(xml::string(node->children->content));
	}
	else if (strequal(node->name, "prefix"))
	{
	  assert(xmlNodeIsText(node->children));
	  prefix(xml::string(node->children->content));
	}
	else if (strequal(node->name, "snippet-prefix"))
	{
	  assert(xmlNodeIsText(node->children));
	  snippet_prefix(xml::string(node->children->content));
	}
	else if (strequal(node->name, "snippet-extension"))
	{
	  assert(xmlNodeIsText(node->children));
	  snippet_extension(xml::string(node->children->content));
	}
	else if (strequal(node->name, "line-number-regex"))
	{
	  assert(xmlNodeIsText(node->children));
	  line_number_regex(xml::string(node->children->content));
	}
	else if (strequal(node->name, "property-names"))
	{
		chapter_number_propname_ = xml::get_attr_value(node, "chapter-number");
		doxygen_propname_ = xml::get_attr_value(node, "doxygen");
		snippets_propname_ = xml::get_attr_value(node, "snippets");
		config_file_propname_ = xml::get_attr_value(node, "config-file");
	}
	else if (strequal(node->name, "rewrite-line"))
	{
		std::string match, replace;
		for (xmlNode* child = node->children; child != nullptr; child = child->next)
		{
			if (strequal(child->name, "match"))
				match = xml::string(child->children->content);
			else if (strequal(child->name, "replace"))
				replace = xml::string(child->children->content);
			else if (not xmlNodeIsText(child))
				throw std::runtime_error(std::string("Invalid node in configuration file: rewrite-line/") + xml::string(child->name));
		}
		rewrites_.emplace_back(match, std::move(replace));
	}
	else if (not xmlNodeIsText(node))
	  throw std::runtime_error(std::string("Invalid node in configuration file: ") + xml::string(node->name));
  }
}

bool Config::rewrite(std::string& text)
const
{
	for (auto&& rule : rewrites_)
	{
		if (rule.match(text))
			return true;
	}
	return false;
}

bool RewriteRule::match(std::string& text)
const
{
	std::smatch match;
	if (std::regex_search(text, match, regex_))
	{
		text = match.prefix().str() + match.format(replace_) + match.suffix().str();
		return true;
	}
	return false;
}
