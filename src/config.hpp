/***************************************************************************
 *   Copyright (C) 2006 by Ray Lischner                                    *
 *   codex@tempest-sw.com                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef CONFIG_HPP
#define CONFIG_HPP

#include "xml.hpp"

#include <regex>
#include <string>
#include <string_view>
#include <unordered_map>
#include <unordered_set>

class RewriteRule {
public:
	RewriteRule(std::string const& match, std::string replace)
	: regex_{match}, replace_{std::move(replace)}
	{}

	bool match(std::string& line) const;

private:
	std::regex regex_;
	std::string replace_;
};

/** @file config.hpp
 * Manage the configuration file. The configuration file is an XML file.
 * It contains a list of the style names for code styles and caption styles,
 * plus file name prefixes and extensions, and the comment string.
 */
class Config {
public:
  using styleset = std::unordered_set<std::string>; ///< Set of style names
  static char const default_configfile[]; ///< Name of default configuration file in system_dir
  static char const schema[];             ///< Text of the configuration schema (using relax-ng)
  static char const version[];            ///< Version number of the configuration file
  static char const system_dir[];         ///< Directory where configuration files are stored
  static char const default_file_tag[];   ///< Default value for file_tag

  /// Construct a configuration object by reading a configuration file.
  /// @param filename name of the configuration file
  Config(char const* filename = default_configfile);

  /// Open a configuration file.
  /// @param filename name of the configuration file
  void open(char const* filename);
  /// Open a configuration file.
  /// @param filename name of the configuration file
  void open(std::string const& filename) { open(filename.c_str()); }

  /// Rewrite a paragraph.
  /// @param[in,out] text The text of the paragraph.
  /// @return true if @p text was modified or false if it was not changed
  bool rewrite(std::string& text) const;

  /// Test whether a style name is a code style. Comparison is case-sensitive.
  /// @param name the style name
  /// @returns true if @p name names a code style, false otherwise.
  bool is_code_style(std::string const& name) const { return is_style(name, code_styles_); }
  /// Test whether a style name is a caption style. Comparison is case-sensitive.
  /// @param name the style name
  /// @returns true if @p name names a caption style, false otherwise.
  bool is_caption_style(std::string const& name) const { return is_style(name, caption_styles_); }
  /// Test whether to ignore a style.
  /// @param name the style name
  /// @returns true if @p name names a style that is in the ignore list, false otherwise.
  bool is_ignore_style(std::string const& name) const { return is_style(name, ignore_styles_); }

  /// Fetch the comment start text. The comment string is empty if
  /// the configuration omits the @c \<comment\> tag.
  /// @returns comment start text
  std::string const& comment_start()   const { return comment_start_; }
  /// Fetch the comment end text. If empty, end the comment with a newline.
  /// @returns comment end text
  std::string const& comment_end()   const { return comment_end_; }
  /// Fetch the doxygen flag.
  /// @returns doxygen flag
  bool doxygen()                     const { return doxygen_; }
  /// Fetch the default file name extension. The extension must include a dot (.)
  /// if one is needed, e.g., ".cpp".
  /// @returns extension text
  std::string const& extension() const { return ext_; }
  /// Fetch the file tag. The default is @c \@file.
  /// @returns file tag
  std::string const& file_tag()   const { return file_tag_; }
  /// Fetch the default file name prefix.
  /// @returns prefix text
  std::string const& prefix()    const { return prefix_; }
  /// Fetch the default file name extension for snippets. The extension must include a dot (.)
  /// if one is needed, e.g., ".cpp".
  /// @returns snippet extension text
  std::string const& snippet_extension()    const { return snippet_extension_; }
  /// Fetch the default file name prefix for snippets.
  /// @returns snippet prefix text
  std::string const& snippet_prefix()    const { return snippet_prefix_; }
  /// Fetch the regex for matching line numbers.
  /// @returns the line number regex
  std::regex const& line_number_regex() const { return line_number_regex_; }
  /// Fetch the name of the chapter number property.
  std::string const& chapter_number_propname() const { return chapter_number_propname_; }
  /// Fetch the name of the doxygen property.
  std::string const& doxygen_propname() const { return doxygen_propname_; }
  /// Fetch the name of the snippets property.
  std::string const& snippets_propname() const { return snippets_propname_; }
  /// Fetch the name of the configuration file property.
  std::string const& config_file_propname() const { return config_file_propname_; }

  /// Add a style name as a code style.
  /// @param name the style name
  void add_code_style(std::string const& name) { add_style(name, code_styles_); }
  /// Add a style name as a caption style.
  /// @param name the style name
  void add_caption_style(std::string const& name) { add_style(name, caption_styles_); }
  /// Add a style name as an ignore style.
  /// @param name the style name
  void add_ignore_style(std::string const& name) { add_style(name, ignore_styles_); }
  /// Set the comment start text.
  /// @param s the new comment start text
  void comment_start(std::string const& s)   { comment_start_ = s; }
  /// Set the comment end text.
  /// @param s the new comment end text
  void comment_end(std::string const& s)   { comment_end_ = s; }
  /// Set the doxygen flag.
  /// @param f the new flag
  void doxygen(bool f)   { doxygen_ = f; }
  /// Set the file tag.
  /// @param s the new file tag
  void file_tag(std::string const& s)   { file_tag_ = s; }
  /// Set the default file name extension.
  /// @param s the new extension
  void extension(std::string const& s) { ext_ = s; }
  /// Set the default file name prefix.
  /// @param s the new prefix
  void prefix(std::string const& s)    { prefix_ = s; }
  /// Set the default file name extension for snippets.
  /// @param s the new snippet extension
  void snippet_extension(std::string const& s)    { snippet_extension_ = s; }
  /// Set the default file name prefix for snippets.
  /// @param s the new snippet prefix
  void snippet_prefix(std::string const& s)    { snippet_prefix_ = s; }
  /// Compiles the line number regex.
  /// @param s the new regex text
  void line_number_regex(std::string const& s)
  {
      line_number_regex_ = std::regex{s};
  }

private:
  /// Test a style name, given a styleset. Look up @p name in @p styles
  /// and return true if found.
  /// @param name the style name
  /// @param styles the set of style names to search
  /// @returns true if @p name is found in @p styles, false otherwise
  bool is_style(std::string const& name, styleset const& styles) const;
  /// Add a style name to a style set.
  /// @param name the style name
  /// @param styles the set of style names to search
  void add_style(std::string const& name, styleset& styles);

  /// Read the configuration data from the XML tree and set the various
  /// data members of @c this.
  /// @param doc the XML document in memory
  void load_config_data(xml::doc& doc);

  /// Clear all data members to empty defaults.
  void clear();

  std::string comment_end_;              ///< comment end text
  std::string comment_start_;            ///< comment start text
  bool doxygen_;                         ///< flag to use doxygen file comments
  std::string file_tag_;                 ///< file tag for comments
  std::string ext_;                      ///< default file name extension for listings
  std::string prefix_;                   ///< default file name prefix for listings
  std::string snippet_extension_;        ///< default file name extension for snippets
  std::string snippet_prefix_;           ///< default file name prefix for snippets
  std::string chapter_number_propname_;  ///< Name of the chapter number property
  std::string doxygen_propname_;         ///< Name of the doxygen property
  std::string snippets_propname_;        ///< Name of the snippets property
  std::string config_file_propname_;     ///< Name of the config file property
  std::regex line_number_regex_;         ///< regex for matching line number in code listings
  std::vector<RewriteRule> rewrites_;	 ///< series of rewrite rules
  styleset code_styles_;                 ///< set of style names for code styles
  styleset caption_styles_;              ///< set of style names for caption styles
  styleset ignore_styles_;               ///< set of style names to ignore
};

#endif
