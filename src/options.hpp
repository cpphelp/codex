/***************************************************************************
 *   Copyright (C) 2006 by Ray Lischner                                    *
 *   codex@tempest-sw.com                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef OPTIONS_HPP
#define OPTIONS_HPP

#include <memory>
#include <string>

#include "config.hpp"

/** @file options.hpp
Global options.
*/

/// Keep track of command line options.
class Options
{
public:
  /// Constructor sets up default values.
  Options() :
	config_(nullptr),
	comment_start_(),
	comment_end_(),
	configfile_(),
	extension_(),
	file_tag_(),
	snippet_extension_(),
	snippet_prefix_(),
	prefix_(),
	chapter_(-1),
	verbose_(1),
	doxygen_(false),
	setdoxygen_(false),
	snippets_(true),
	warning_(true)
  {}

  /// Fetch configuration object. If the object has not yet been created,
  /// that means configfile has never been called, so the default configuration
  /// will be created.
  /// @returns the sole instance of Config
  Config& config() const;

  /// Fetch comment end text.
  /// @returns the comment end text
  std::string comment_end() const;
  /// Set comment end text.
  /// @param c the new comment end text
  void comment_end(std::string const& c)    { comment_end_ = c; }

  /// Fetch comment start text.
  /// @returns the comment start text
  std::string const& comment_start() const;
  /// Set comment start text.
  /// @param c the new comment start text
  void comment_start(std::string const& c)    { comment_start_ = c; }

  /// Fetch the name of the configuration file.
  /// @returns the configuration file name
  std::string const& configfile() const { return configfile_; }
  /// Sets the configuration file name and loads the file into a Config object.
  /// @param cf name of the configuration file.
  /// @see Config
  void configfile(std::string const& cf);

  /// Fetch the doxygen flag.
  /// @returns the doxygen flag
  bool doxygen() const;
  /// Set the doxygen flag.
  /// @param d the new doxygen flag
  void doxygen(bool d)  { doxygen_ = d; setdoxygen_ = true; }

  /// Fetch default extension for listings.
  /// @returns the default extension
  std::string const& extension() const;
  /// Set extension for listings.
  /// @param e the new extension
  void extension(std::string const& e)  { extension_ = e; }

  /// Fetch file tag.
  /// @returns the file tag string
  std::string const& file_tag() const;
  /// Set file tag.
  /// @param s the new file tag
  void file_tag(std::string const& s)    { file_tag_ = s; }

  /// Fetch default prefix for listings.
  /// @returns the default prefix
  std::string const& prefix()    const;
  /// Set prefix for listings.
  /// @param p the new prefix
  void prefix(std::string const& p)     { prefix_ = p; }

  /// Fetch the flag that indicates whether to extract snippets.
  /// @returns snippets flag
  bool snippets()                const { return snippets_; }
  /// Set the snippets flag.
  /// @param s the new flag
  void snippets(bool s)                { snippets_ = s; }

  /// Fetch default extension for snippets.
  /// @returns the default extension
  std::string const& snippet_extension() const;
  /// Set extension for snippets.
  /// @param e the new extension
  void snippet_extension(std::string const& e) { snippet_extension_ = e; }

  /// Fetch default prefix for snippets.
  /// @returns the default prefix
  std::string const& snippet_prefix() const;
  /// Set prefix for snippets.
  /// @param p the new prefix
  void snippet_prefix(std::string const& p) { snippet_prefix_ = p; }


  /// Fetch the chapter number. A negative number means no chapter number is set.
  /// @returns the chapter number
  int                chapter()   const { return chapter_; }
  /// Set chapter number.
  /// @param c the chapter number
  void chapter(int c)                  { chapter_ = c; }
  /// Set chapter number. The string is converted to an integer.
  /// No error checking is performed; if the string is invalid, 0 is used.
  /// @param arg the chapter number as text
  void chapter(char const* arg);
  /// Indicates whether the chapter number is set.
  /// @returns true if the chapter number is set, that is, is non-negative;
  /// false if the chapter number is not set (negative).
  bool have_chapter()            const { return chapter_ >= 0; }

  /// Fetch the verbosity. Zero means be quiet. Small integers mean
  /// be verbose; larger values mean more verbose. Specifically:
  /// 1 means echo document names; 2 means also echo generated files;
  /// 3 means echo captions for listing files.
  /// @returns the current verbosity
  int               verbose()    const { return verbose_; }
  /// Increment the verbosity level. Each time the @c --verbose command line
  /// option is used on the command line, the verbosity increases.
  void increment_verbose()             { ++verbose_; }
  /// Clear the verbosity, that is, be quiet.
  void be_quiet()                      { verbose_ = 0; }

  /// Fetch the warning flag.
  /// @returns true if chapter and listing numbers are to be checked; false if not
  bool               warning()   const { return warning_; }
  /// Set the warning flag
  /// @param w the new value of the warning flag
  void warning(bool w)                 { warning_ = w; }

private:
  mutable std::unique_ptr<Config> config_; ///< The sole Config instance
  std::string comment_start_;            ///< The comment start text
  std::string comment_end_;              ///< The comment end text
  std::string configfile_;               ///< The name of the configuration file
  std::string extension_;                ///< The extension for listings
  std::string file_tag_;                 ///< The tag to use for file comments
  std::string snippet_extension_;        ///< The extension for snippets
  std::string snippet_prefix_;           ///< The prefix for snippets
  std::string prefix_;                   ///< The prefix for listings
  int chapter_;                          ///< The chapter number (negative means no chapter)
  int verbose_;                          ///< The verbosity (zero is quiet; higher numbers mean more verbose)
  bool doxygen_;                         ///< The string that appears in a document-file comment
  bool setdoxygen_;                      ///< the doxygen flag has been set
  bool snippets_;                        ///< flag for extracting snippets
  bool warning_;                         ///< The warning flag
};

/// The single global options object.
extern Options options;

#endif
